var Beacon = require('../models/beacon');

module.exports = function (io) {
  'use strict';
  io.on('connection', function (socket) {
    console.log("new connection");

    socket.on('test', function (data) {
      console.log("test with: "+data);
      io.emit('test:test', data);

    });

    socket.on('beacons', function(from, beacons){
      var beacon = new Beacon({
        user: from,
        beacons: beacons
      });
      beacon.save(function (err) {
        if (err){
          console.error(err);
        } else {
          //console.log("Beacons saved")
        }
      });

      console.log(from+" has detected beacons");
      io.emit('beacon:detect',{
        id: from,
        beacons: beacons
      });
    });

    socket.on('enter', function(from, beacon){
      console.log(from+" has entered beacon: "+beacon.major);
      io.emit('beacon:enter',{
        id: from,
        beacon: beacon
      });
    });

    socket.on('left', function(from, beacon){
      //console.log(from+" has left beacon: "+beacon.major);
      io.emit('beacon:left',{
        id: from,
        beacon: beacon
      });
    });
  
    socket.on('newnearest', function(from, nearestBeacon, beaconsSince){
      console.log(from+" detected a new nearest beacon: "+nearestBeacon.major);
  
      for (var i = 0; i < beaconsSince.length; i++) {
        var b = beaconsSince[i];
        b.user = from;
        Beacon.create(b, function(err, beacon){
          if (err) throw err;
        });
      }
      
      io.emit('beacon:newnearest',{
        id: from,
        beacon: nearestBeacon
      });
    });
  });
};