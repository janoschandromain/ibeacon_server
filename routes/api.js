var express = require('express');
var router = express.Router();
var Beacon = require('../models/beacon');

/* GET home page. */
router.get('/getall', function(req, res, next) {
  Beacon.find({},function (err, beacons) {
    if (err){
      console.error(err);
      res.send(err);
    } else {
      res.json(beacons);
    }
  });
});

module.exports = router;
