var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BeaconData = new Schema({
  'user': String,
  //'timestamp' : { type : Date, default: Date.now },
  'beacons': {}
});

module.exports = mongoose.model('BeaconData', BeaconData);
