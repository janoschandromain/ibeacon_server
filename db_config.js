var db = {
  db: 'ibeacon'
  ,host: 'localhost'
  ,port: 27017 //20762
  //,username: 'ibeacon'
  //,password: 'ibeaconpwd'
};

var dbUrl = 'mongodb://';
//dbUrl += db.username+':'+db.password+'@';
dbUrl += db.host + ':' + db.port;
dbUrl += '/' + db.db;

exports.db = {
  db: db,
  secret : 'megasecret',
  url: dbUrl
};